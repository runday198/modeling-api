### Know which shelf the book is on

| GET                       | /books                                                                                                                   |
| ------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| Can Access                | Librarian or Reader                                                                                                      |
| Params Description        | No params                                                                                                                |
| Query Params Description  | isbn (optional) - the ISBN code of the book, name (optional) - the name of the book, genre (optional), author (optional) |
| Request Body              | None                                                                                                                     |
| Responses:                |
| 200 OK                    | Successfully fetched book(s) location                                                                                    |
| 500 Internal Server Error | Something went wrong on the server                                                                                       |
| Response Body on success  |

```

  [
    {
      id: string,
      isbn: number,
      name: string,
      genre: string,
      author: string,
      shelfId: string
    },
    {
      id: string,
      isbn: number,
      name: string,
      genre: string,
      author: string,
      shelfId: string
    }
  ]

```

---

---

### Put a new book on the shelf

| POST                      | /books/book                                                                  |
| ------------------------- | ---------------------------------------------------------------------------- |
| Can Access                | Librarian                                                                    |
| Params Description        | None                                                                         |
| Query Params Description  | No query params                                                              |
| Request Body              | {isbn: number, name: string, author: string, genre: string, shelfId: string} |
| Responses:                |
| 200 OK                    | The book was successfully added to the shelf                                 |
| 404 Not Found             | Either the book or the shelf were not found                                  |
| 500 Internal Server Error | Something went wrong on the server                                           |
| Response Body             | None                                                                         |

---

---

### Move a book between shelves

| PUT                       | /books/:bookId/move                                                     |
| ------------------------- | ----------------------------------------------------------------------- |
| Can Access                | Librarian                                                               |
| Params Description        | bookId - the ID of the book being moved                                 |
| Query Params Description  | None                                                                    |
| Request Body              | { sourceShelfId: string, targetShelfId: string }                        |
| Responses:                |
| 200 OK                    | The books was successfully moved                                        |
| 404 Not Found             | Either the book, or the source shelf or the target shelf were not found |
| 500 Internal Server Error | Something went wrong on the server                                      |
| Response Body             | None                                                                    |

---

---

### Get information on a book using its ID

| GET                       | /books/:bookId                                        |
| ------------------------- | ----------------------------------------------------- |
| Params Description        | bookId - The id of the book.                          |
| Query Params Description  | None                                                  |
| Request Body              | None                                                  |
| Responses:                |
| 200 OK                    | Successfully retrieved the information about the book |
| 404 Not Found             | The book was not found                                |
| 500 Internal Server Error | Something went wrong on the server                    |
| Response Body             |

```
{
    id: string,
    isbn: number,
    name: string,
    author: string,
    genre: string,
    shelfId: string
}
```

---

---

### Take a book

| PUT                       | /books/:bookId/take                      |
| ------------------------- | ---------------------------------------- |
| Can Access                | Reader                                   |
| Params Description        | bookId - The id of the book being taken  |
| Query Params              | None                                     |
| Request Body              | { returnDate: string, readerId: string } |
| Responses:                |
| 200 OK                    | The book was successfully borrowed       |
| 404 Not Found             | The book was not found                   |
| 500 Internal Server Error | Something went wrong on the server       |
| Response Body             | None                                     |

---

---

### Reserve a book

| POST                      | /books/:bookId/reserve                            |
| ------------------------- | ------------------------------------------------- |
| Can Access                | Reader                                            |
| Params Description        | bookId - the ID of the book                       |
| Query Params              | None                                              |
| Request Body              | { reservationPeriod: string }                     |
| Responses                 |
| 200 OK                    | The book was successfully reserved                |
| 404 Not Found             | The book was not found                            |
| 400 Bad Request           | The book is already reserved for the given period |
| 500 Internal Server Error | Something went wrong on the server                |
| Response Body             |

```
[
  {
    isbn: number,
    id: string,
    name: string,
    author: string,
    genre: string,
    shelfId: string
  },
  {
    isbn: number,
    id: string,
    name: string,
    author: string,
    genre: string,
    shelfId: string
  }
]

```

---

---

### Get all the books on a specific shelf

| GET                       | /shelves/:shelfId/books                            |
| ------------------------- | -------------------------------------------------- |
| Can Access                | Librarian or Reader                                |
| Params Description        | shelfId - the ID of the shelf                      |
| Query Params Description  | None                                               |
| Responses:                |
| 200 OK                    | The books on the shelf were successfully retrieved |
| 404 Not Found             | The shelf was not found                            |
| 500 Internal Server Error | Something went wrong on the server                 |
| Response Body:            |

```
{
  shelfId: string,
  books: [
    {
      id: string,
      isbn: number,
      name: string,
      genre: string,
      author: string,
      shelfId: string
    },
    {
      id: string,
      isbn: number,
      name: string,
      genre: string,
      author: string,
      shelfId: string
    }
  ]
}

```
